// timemenu.cc for bbtime - an tool for display the time.
//
//  Copyright (c) 1998,1999 by John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//
#include "timemenu.hh"

Timemenu::Timemenu(ToolWindow *toolwindow) :
	Basemenu(toolwindow)
{
	int i;
	bbtool=toolwindow;
	
	setTitleVisibility(False);
	setMovable(False);
	setHidable(False);
	setAlignment(Basemenu::MenuAlignBottom);
	defaultMenu();

	word_length = new int [bbtool->getResource()->number_of_times];
	for (i=0;i<bbtool->getResource()->number_of_times;i++)
	{
		insert(bbtool->getResource()->timefriend[i].name);
		word_length[i]=strlen(bbtool->getResource()->timefriend[i].name);
	}
	insert("Reconfigure");
}


Timemenu::~Timemenu(void)
{
	delete [] word_length;
}

void Timemenu::ClearMenu()
{
	int i;
	
	for (i=0;i<=bbtool->getResource()->number_of_times;i++)
		remove(0);
	delete [] word_length;
}

void Timemenu::Reconfigure()
{
	int i;

	word_length = new int [bbtool->getResource()->number_of_times];
	for (i=0;i<bbtool->getResource()->number_of_times;i++)
	{
		insert(bbtool->getResource()->timefriend[i].name,0);
		word_length[i]=strlen(bbtool->getResource()->timefriend[i].name);
	}
	insert("Reconfigure");	
	Basemenu::Reconfigure();
}


void Timemenu::Move(int x, int y, bool withdrawn)
{
	if (!withdrawn)
	{
		y= y - Height() -1;
		if ((x+Width())>bbtool->xRes())
			x=bbtool->xRes()-Width();
        if (y<0)
	       	y=bbtool->Frame().height - 1;
	    Basemenu::Move(x, y);
	}
	else
	{
		x= x - Width() - 6;
		if (x<0)
			x=x + Width() + 12;
     	if (y<0)
           	y=0;
												
		Basemenu::Move(x, y);
	}
}

/* quick and dirty */
void Timemenu::UpdateTime( time_t tmp,int number)
{
  struct tm *tx=0;
  
  bbtool->getResource()->timefriend[number].name[word_length[number]]='\0';

  if (! (tx = localtime(&tmp))) return;
#ifdef    HAVE_STRFTIME
  char t[1024];
  if (! strftime(t, 1024,bbtool->getResource()->report.strftimeFormat, tx))
    return;
#else // !HAVE_STRFTIME
  char t[9];
  if (date) {
    // format the date... with special consideration for y2k ;)
    if (resource->report.euStyle)
      sprintf(t, "%02d.%02d.%02d", tx->tm_mday, tx->tm_mon + 1,
              (tx->tm_year >= 100) ? tx->tm_year - 100 : tx->tm_year);
    else
      sprintf(t, "%02d/%02d/%02d", tx->tm_mon + 1, tx->tm_mday,
              (tx->tm_year >= 100) ? tx->tm_year - 100 : tx->tm_year);
  } else {
    if (resource->report.clock24Hour)
    	sprintf(t, "  %02d:%02d ", hour, minute);
    else
    	sprintf(t, "%02d:%02d %cm",
   	((hour > 12) ? hour - 12 :
    ((hour == 0) ? 12 : hour)), minute,
 	  ((hour >= 12) ? 'p' : 'a'));
  }
#endif // HAVE_STRFTIME
  strcat(bbtool->getResource()->timefriend[number].name," ");
  strcat(bbtool->getResource()->timefriend[number].name,t);
  wait_for_update=True;
}

void Timemenu::Update()
{
	wait_for_update=False;
	Basemenu::Update();
}

void Timemenu::itemSelected(int button, int index)
{
  if (button == 1)
  {
    if (index < bbtool->getResource()->number_of_times)
    {
      bbtool->setFriendSelect(index);
      bbtool->CheckTime(True);
      setHighlight(index);
    }
    else if (index == bbtool->getResource()->number_of_times)
    {
  		bbtool->Reconfigure();
    }
    Hide();
//    bbtool->EnvelopePushed(False);
    bbtool->Redraw();
  }
}

void Timemenu::Show(void) {
  XRaiseWindow(bbtool->getDisplay(), WindowID());
  Basemenu::Show();
}
