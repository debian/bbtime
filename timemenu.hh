// timemenu.hh for bbtime - an tool for display the time in X11.
//
//  Copyright (c) 1998,1999 by John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//
#ifndef __TIMEMENU_HH
#define __TIMEMENU_HH

#include "Basemenu.hh"
#include "bbtime.hh"

class Basemenu;
class ToolWindow;

class Timemenu : public Basemenu
{
	public:
		Timemenu(ToolWindow *);
		~Timemenu(void);
	    void UpdateTime(time_t, int);
	    void Update(void);
	    bool WaitForUpdate(void) {return wait_for_update;}
	    void Move(int ,int, bool);
	    void Reconfigure(void);
	    void ClearMenu(void);
	    void Clean(void);
      virtual void Show(void);

	protected:
		virtual void itemSelected(int, int);		
		
	private:
		ToolWindow *bbtool;
		int *word_length;
		bool wait_for_update;

};

#endif /* __TIMEMENU_HH */
