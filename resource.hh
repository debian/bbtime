// resource.hh for bbtime - an tool to display the time in X11.
//
//  Copyright (c) 1998-1999 John Kennis, jkennis@chello.nl
//
//  this program is free software; you can redistribute it and/or modify
//  it under the terms of the gnu general public license as published by
//  the free software foundation; either version 2 of the license, or
//  (at your option) any later version.
//
//  this program is distributed in the hope that it will be useful,
//  but without any warranty; without even the implied warranty of
//  merchantability or fitness for a particular purpose.  see the
//  gnu general public license for more details.
//
//  you should have received a copy of the gnu general public license
//  along with this program; if not, write to the free software
//  foundation, inc., 675 mass ave, cambridge, ma 02139, usa.
//
// (see the included file copying / gpl-2.0)
//


#ifndef __RESOURCE_HH
#define __RESOURCE_HH

#include "bbtime.hh"
#include "Baseresource.hh"

#define BBTOOLS 1
#define DEFAULT 2
#define BLACKBOX 3

class BaseResource;

struct FRAME {
  int width;
  int height;
  BTexture texture;
  int bevelWidth;
  XFontStruct *font;
};

struct POSITION {
  int x;
  int y;
  int mask;
};

struct LABEL {
  int width;
  int width0, width1, width2, width3;
  int height;
  bool transparent;
  BTexture texture;
  XFontStruct *font;
  BColor textColor;
};


struct SHOW {
  bool newmail_counter;
  bool totalmail_counter;
  bool label;
  bool envelope;
  bool onlyAtNewMail;
};

struct REPORT {
  bool auto_raise;
  unsigned int check_delay;
  bool euStyle;
  bool clock24Hour;
  char *strftimeFormat;
};

struct MENU {
	BTexture texture;
	BColor textColor;
	BColor highlightColor;
	BColor hitextColor;
	int justify;
	XFontStruct *font;
};

struct TIMEINFO {
  char *name;
  long int offset;
};


class Resource : public BaseResource
{

  public:
  	Resource(ToolWindow *);
	~Resource(void);

	struct FRAME frame; 
	struct LABEL label;
	struct POSITION position;
	struct SHOW show;
	struct MENU menu;
	struct REPORT report;
  struct TIMEINFO *timefriend;
  int number_of_times;

  protected:
  	virtual void  LoadBBToolResource(void);

  private:

  int time_len;
	void Frame(void);
	void SizeAndPosition(void);
	void MailCheckSettings(void);
  void getTimeInfo(int);
  void TimeInfo(void);
	void Label(void);
	void Show(void);
	void Menu(void);
	void Envelope(void);
	void MailboxDep(void);
	
	void Clean(void);
};
#endif /* __RESOURCE_HH */
