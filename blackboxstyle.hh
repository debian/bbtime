//  blackboxstyle.hh for bbtools.
//
//  Copyright (c) 1998-1999 by John Kennis, jkennis@chello.nl
//
//  this program is free software; you can redistribute it and/or modify
//  it under the terms of the gnu general public license as published by
//  the free software foundation; either version 2 of the license, or
//  (at your option) any later version.
//
//  this program is distributed in the hope that it will be useful,
//  but without any warranty; without even the implied warranty of
//  merchantability or fitness for a particular purpose.  see the
//  gnu general public license for more details.
//
//  you should have received a copy of the gnu general public license
//  along with this program; if not, write to the free software
//  foundation, inc., 675 mass ave, cambridge, ma 02139, usa.
//
// (see the included file copying / gpl-2.0)
//


#ifndef __BLACKBOXSTYLE_HH
#define __BLACKBOXSTYLE_HH

/* bbdate.frame */
#define BB_FRAME "toolbar"
/* bbdate.label */
#define BB_LABEL "toolbar.label"
/* bbdate.textColor */
#define BB_LABEL_TEXTCOLOR "toolbar.textColor"
/* bbdate.bevelWidth */
#define BB_BEVELWIDTH "bevelWidth"
/* bbdate.*.font */
#define BB_FONT "titleFont"
/* bbmail.menu.frame */
#define BB_MENU "menu.frame"
/* bbmail.menu.highlight.color */
#define BB_MENU_HIGHLIGHT_COLOR "menu.frame.highlightColor"
/* bbmail.menu.textColor */
#define BB_MENU_TEXTCOLOR "menu.frame.textColor"
/* bbmail.menu.highlight.textColor */
#define BB_MENU_HITEXTCOLOR "menu.frame.hiTextColor"
/* bbmail.menuJustify */
#define BB_MENU_JUSTIFY "menuJustify"
/* bbmail.menuFont */
#define BB_MENU_FONT "menuFont"


#endif /* __BLACKBOXSTYLE_HH */
