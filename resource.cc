// resource.cc for bbtime - an tool to display  the time in X11.
//
//  Copyright (c) 1998-1999 John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//

#include "resource.hh"
#include "blackboxstyle.hh"

Resource::Resource(ToolWindow *toolwindow): 
  BaseResource(toolwindow)
{
  report.strftimeFormat=NULL;
  frame.font=label.font=menu.font=0;
  frame.texture.color.allocated=
  frame.texture.colorTo.allocated=
  frame.texture.hiColor.allocated=
  frame.texture.loColor.allocated=
  label.texture.color.allocated=
  label.texture.colorTo.allocated=
  label.texture.hiColor.allocated=
  label.texture.loColor.allocated=
  menu.texture.color.allocated=
  menu.texture.colorTo.allocated=
  menu.texture.hiColor.allocated=
  menu.texture.loColor.allocated=
	label.textColor.allocated=
  menu.textColor.allocated=
  menu.highlightColor.allocated=
  menu.hitextColor.allocated=
  0;


  Load();
}

Resource::~Resource()
{
  frame.font=0;
  label.font=0;
  menu.font=0;
	Clean();
}

void Resource::Clean()
{
	if (label.font) 
		XFreeFont(bbtool->dpy, label.font);
	if (frame.font) 
		XFreeFont(bbtool->dpy, frame.font);
 	if (menu.font)
		XFreeFont(bbtool->dpy, menu.font);

  frame.font=label.font=menu.font=0;	
}


void Resource::LoadBBToolResource(void)
{
	XrmValue value;
	char *value_type;

	if (XrmGetResource(resource_db, "bbtime.autoConfig",
		     "Bbtime.Autoconfig", &value_type, &value))
	{
    	if (! strncasecmp("true", value.addr, value.size))
    	{
				style.auto_config = True;
  		}
	    else
    		style.auto_config = False;
	}
	else
		style.auto_config = False;

  SizeAndPosition();
	
	Frame();
	Show();
	Menu();
	Label();
  TimeInfo();
}

void Resource::Menu()
{
	XrmValue value;
	char *value_type;

	
	readDatabaseTexture("bbtime.menu","Bbtime.Menu",BB_MENU,"Menu.Frame",
                     	"slategrey","darkslategrey",
                      BImage_Raised|BImage_Diagonal|BImage_Gradient|
                      BImage_Bevel1,&menu.texture);
															   	
  readDatabaseColor("bbtime.menu.highlight.color",
				            "Bbtime.Menu.Highlight.Color",
                     BB_MENU_HIGHLIGHT_COLOR,"Menu.highlightColor",
                     "rgb:c/9/6",&menu.highlightColor);
  
	readDatabaseColor("bbtime.menu.textColor","Bbtime.Menu.TextColor",
							      BB_MENU_TEXTCOLOR,"Menu.Frame.TextColor",
                    "LightGrey",&menu.textColor);

	readDatabaseColor("bbtime.menu.highlight.textColor",
	      						"Bbtime.Menu.Highlight.TextColor",
				      			BB_MENU_HITEXTCOLOR,"Menu.Frame.HiTextColor",
                    "white",&menu.hitextColor);

  if (XrmGetResource(resource_db,"bbtime.menuJustify","Bbtime.MenuJustify",
						&value_type, &value))
	{
    	if (! strncasecmp("leftjustify", value.addr, value.size))
	      menu.justify = ToolWindow::B_LeftJustify;
    	else if (! strncasecmp("rightjustify", value.addr, value.size))
	      menu.justify = ToolWindow::B_RightJustify;
	    else if (! strncasecmp("centerjustify", value.addr, value.size))
	      menu.justify = ToolWindow::B_CenterJustify;
	    else
    	  menu.justify = ToolWindow::B_LeftJustify;
	  }
	  else if (XrmGetResource(resource_db,BB_MENU_JUSTIFY,"Bbtime.MenuJustify",
								&value_type, &value))
	  {
    	if (! strncasecmp("leftjustify", value.addr, value.size))
	      menu.justify = ToolWindow::B_LeftJustify;
    	else if (! strncasecmp("rightjustify", value.addr, value.size))
	      menu.justify = ToolWindow::B_RightJustify;
	    else if (! strncasecmp("centerjustify", value.addr, value.size))
	      menu.justify = ToolWindow::B_CenterJustify;
	    else
    	  menu.justify = ToolWindow::B_LeftJustify;	
	  }
	  else
	    menu.justify = ToolWindow::B_LeftJustify;

	const char *defaultFont = "-*-helvetica-medium-r-*-*-*-120-*-*-*-*-*-*";
	
	if (menu.font)
	{
		XFreeFont(bbtool->dpy, menu.font);
		menu.font = 0;
	}
	
	if (XrmGetResource(resource_db, "bbtime.menu.font", "Bbtime.Menu.Font",
						&value_type, &value))
	{
		if ((menu.font = XLoadQueryFont(bbtool->dpy, value.addr)) == NULL)
		{
    	fprintf(stderr, " blackbox: couldn't load font '%s'\n"
	      							" ...  reverting to default font.", value.addr);
		    if ((menu.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
			{
				fprintf(stderr,
					"blackbox: couldn't load default font.  please check to\n"
					"make sure the necessary font is installed '%s'\n",
					defaultFont);
			exit(2);
			}
		}
	}
	else if  (XrmGetResource(resource_db, BB_MENU_FONT, "MenuFont", &value_type, &value))
	{
		if ((menu.font = XLoadQueryFont(bbtool->dpy, value.addr)) == NULL)
		{
			fprintf(stderr, " blackbox: couldn't load font '%s'\n"
	      					" ...  reverting to default font.", value.addr);
			if ((menu.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
			{
				fprintf(stderr,
					"blackbox: couldn't load default font.  please check to\n"
					"make sure the necessary font is installed '%s'\n",
					defaultFont);
				exit(2);
			}
		}
	}
	else
	{
		if ((menu.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
		{
			fprintf(stderr,"blackbox: couldn't load default font.  please check to\n"
				    "make sure the necessary font is installed '%s'\n", defaultFont);
			exit(2);
		}
	}	
}


void Resource::Frame()
{
	XrmValue value;
	char *value_type;

  readDatabaseTexture("bbtime.frame","Bbtime.Frame",BB_FRAME,"Toolbar",
                      "slategrey","darkslategrey",
                      BImage_Raised|BImage_Gradient|BImage_Vertical|
                      BImage_Bevel1,&frame.texture);
	
	if (XrmGetResource(resource_db, "bbtime.bevelWidth","Bbtime.BevelWidth",
						&value_type, &value))
	{
		if (sscanf(value.addr, "%u", &frame.bevelWidth) != 1)
			frame.bevelWidth = 4;
		else if (frame.bevelWidth == 0)
			frame.bevelWidth = 4;
	}
	else if (XrmGetResource(resource_db, BB_BEVELWIDTH,"BevelWidth", &value_type,
                          &value))
	{
		if (sscanf(value.addr, "%u", &frame.bevelWidth) != 1)
			frame.bevelWidth = 4;
		else if (frame.bevelWidth == 0)
			frame.bevelWidth = 4;
	}
	else
    	frame.bevelWidth = 4;
}


void Resource::SizeAndPosition()
{
	XrmValue value;
	char *value_type;
	unsigned int w,h;
	char positionstring[11];

  if (!(bbtool->withdrawn))
  {
  	if (XrmGetResource(resource_db, "bbtime.withdrawn",
		     "Bbtime.Withdrawn", &value_type, &value))
    {
  		if (! strncasecmp("true", value.addr, value.size))
	  		bbtool->withdrawn = True;
	    else
		  	bbtool->withdrawn = False;
  	}	
	  else
		  bbtool->withdrawn = False;
  }

  if (!(bbtool->shape))
  {
  	if (XrmGetResource(resource_db, "bbtime.shape",
		     "Bbtime.Shape", &value_type, &value))
    {
  		if (! strncasecmp("true", value.addr, value.size))
	  		bbtool->shape = True;
	    else
		  	bbtool->shape = False;
  	}	
	  else
		  bbtool->shape = bbtool->withdrawn;
  }

  
	if (!(bbtool->position))
	{
		if (!(XrmGetResource(resource_db, "bbtime.position","Bbtime.Position",
							&value_type, &value)))
			strncpy(positionstring, "-0-0", 5);
		else
			strncpy(positionstring, value.addr, strlen(value.addr)+1);
	}
	else
		strncpy(positionstring, bbtool->position, strlen(bbtool->position)+1);


	position.mask=XParseGeometry(positionstring, &position.x, &position.y, &w, &h);
	if (!(position.mask & XValue))
		position.x=0;
	if (!(position.mask & YValue))
		position.y=0;

	/* need this to compute the height */
	const char *defaultFont = "-*-helvetica-medium-r-*-*-*-120-*-*-*-*-*-*";
	
	if (frame.font)
	{
    	XFreeFont(bbtool->dpy, frame.font);
		frame.font = 0;
	}
	
	if (XrmGetResource(resource_db, "bbtime.heightBy.font","Bbtime.heightBy.Font",
						&value_type, &value))
	{
    	if ((frame.font = XLoadQueryFont(bbtool->dpy, value.addr)) == NULL)
		{
    		fprintf(stderr, " blackbox: couldn't load font '%s'\n"
	      					" ...  reverting to default font.", value.addr);
	    	if ((frame.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
			{
				fprintf(stderr,"blackbox: couldn't load default font.  please check to\n"
								"make sure the necessary font is installed '%s'\n",
					defaultFont);
				exit(2);
			}
		}
	}
	else if (XrmGetResource(resource_db, BB_FONT,"TitleFont", &value_type, &value))
	{
		if ((frame.font = XLoadQueryFont(bbtool->dpy, value.addr)) == NULL)
		{
    		fprintf(stderr, " blackbox: couldn't load font '%s'\n"
	      					" ...  reverting to default font.", value.addr);
			if ((frame.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
			{
				fprintf(stderr,
					"blackbox: couldn't load default font.  please check to\n"
					"make sure the necessary font is installed '%s'\n",
					defaultFont);
				exit(2);
			}
		}
	}
	else 
	{
    	if ((frame.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
		{
			fprintf(stderr,
	      			"blackbox: couldn't load default font.  please check to\n"
	      			"make sure the necessary font is installed '%s'\n", defaultFont);
			exit(2);
		}
	}
}
	
void Resource::Label(void)
{
	XrmValue value;
	char *value_type;

	/* text-label resources */
	if (XrmGetResource(resource_db, "bbtime.label.transparent",
		     "Bbtime.label.Transparent", &value_type, &value))
	{
		if (! strncasecmp("true", value.addr, value.size))
			label.transparent = True;
	    else
			label.transparent = False;
	}	
	else
		label.transparent = False;
	
	readDatabaseTexture("bbtime.label", "Bbtime.Label",BB_LABEL,"Toolbar.Label",
                      "slategrey","darkslategrey",
                      BImage_Sunken|BImage_Gradient|BImage_Vertical|
                      BImage_Bevel1,&label.texture);

		readDatabaseColor("bbtime..textColor",
      								"Bbtime.TextColor",
                      BB_LABEL_TEXTCOLOR, "Toolbar.TextColor",
                      "LightGrey",&label.textColor);

    
	const char *defaultFont = "-*-helvetica-medium-r-*-*-*-120-*-*-*-*-*-*";
	
	if (label.font)
	{
		XFreeFont(bbtool->dpy, label.font);
		label.font = 0;
	}
	
	if (XrmGetResource(resource_db, "bbtime.label.font", "Bbtime.Label.Font",
						&value_type, &value))
	{
		if ((label.font = XLoadQueryFont(bbtool->dpy, value.addr)) == NULL)
		{
    	fprintf(stderr, " blackbox: couldn't load font '%s'\n"
	      							" ...  reverting to default font.", value.addr);
		    if ((label.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
			{
				fprintf(stderr,
					"blackbox: couldn't load default font.  please check to\n"
					"make sure the necessary font is installed '%s'\n",
					defaultFont);
			exit(2);
			}
		}
	}
	else if  (XrmGetResource(resource_db, BB_FONT, "TitleFont", &value_type, &value))
	{
		if ((label.font = XLoadQueryFont(bbtool->dpy, value.addr)) == NULL)
		{
			fprintf(stderr, " blackbox: couldn't load font '%s'\n"
	      					" ...  reverting to default font.", value.addr);
			if ((label.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
			{
				fprintf(stderr,
					"blackbox: couldn't load default font.  please check to\n"
					"make sure the necessary font is installed '%s'\n",
					defaultFont);
				exit(2);
			}
		}
	}
	else
	{
		if ((label.font = XLoadQueryFont(bbtool->dpy, defaultFont)) == NULL)
		{
			fprintf(stderr,"blackbox: couldn't load default font.  please check to\n"
				    "make sure the necessary font is installed '%s'\n", defaultFont);
			exit(2);
		}
	}
}


void Resource::Show()
{
	XrmValue value;
	char *value_type;

 	if (XrmGetResource(resource_db, "bbtime.show.euStyle",
		     "Bbtime.show.EuStyle", &value_type, &value))
	{
    	if (! strncasecmp("true", value.addr, value.size))
    	{
				report.euStyle = True;
  		}
	    else
    		report.euStyle = False;
	}
	else
		report.euStyle = False;

 	if (XrmGetResource(resource_db, "bbtime.show.clock24Hour",
		     "Bbtime.show.Clock24Hour", &value_type, &value))
	{
    	if (! strncasecmp("true", value.addr, value.size))
    	{
				report.clock24Hour = True;
  		}
	    else
    		report.clock24Hour = False;
	}
	else
		report.clock24Hour = False;

    if (XrmGetResource(resource_db,"bbtime.strftimeFormat",
                       "Bbtime.strftimeFormat",&value_type, &value)) {
    delete [] report.strftimeFormat;
    int len=strlen(value.addr);
    report.strftimeFormat = new char [len +1];
    memset(report.strftimeFormat,0,len+1);
    strncpy(report.strftimeFormat,value.addr,len);
  }
  else {
    delete [] report.strftimeFormat;
    int len=strlen("%I:%M %p");
    report.strftimeFormat = new char [len +1];
    memset(report.strftimeFormat,0,len+1);
    strncpy(report.strftimeFormat,"%I:%M %p",len);
  }

#ifdef    HAVE_STRFTIME
  time_t ttmp = time(NULL);
  struct tm *tt = 0;
  
  if (ttmp != -1) {
    tt = localtime(&ttmp);
    if (tt) {
      char t[1024];// *time_string = (char *) 0;
      time_len = strftime(t, 1024,
                        report.strftimeFormat,tt);
    }
  }
#else // !HAVE_STRFTIME
   time_len=strlen("00:00000"); 
#endif // HAVE_STRFTIME

}

void Resource::getTimeInfo(int nr)
{
	XrmValue value;
	char *value_type;
  char rname[24];
  char rclass[24];
  int hour;
  int minute;

  sprintf(rname,"bbtime.friend.%d.name",nr);
  sprintf(rclass,"Bbtime.Friend.%d.Name",nr);
 
  
   if (XrmGetResource(resource_db,rname,rclass,&value_type, &value)) {
    int len=strlen(value.addr);
    
    timefriend[nr].name = new char [len +2 + time_len];
    memset(timefriend[nr].name,0,len+2+time_len);
    strncpy(timefriend[nr].name,value.addr,len);
  }
  else {
    fprintf(stderr,"Can't read name \n");
    exit(1);
  }

  sprintf(rname,"bbtime.friend.%d.offset",nr);
  sprintf(rclass,"Bbtime.Friend.%d.Offset",nr);

 	if (XrmGetResource(resource_db, rname,rclass,
						&value_type, &value))
	{
		if (sscanf(value.addr, "%i:%i", &hour,&minute) != 2) 
			timefriend[nr].offset=0;
    else { 
      if (hour<0)
        timefriend[nr].offset=(hour*60-minute)*60;
      else
        timefriend[nr].offset=(hour*60+minute)*60;
    }
	}
  else
    timefriend[nr].offset=0; 
}

void Resource::TimeInfo()
{
	XrmValue value;
	char *value_type;
  int i;
  
  
 	if (XrmGetResource(resource_db, "bbtime.numberOf.friends",
                     "Bbtime.NumberOf.Friends",
          						&value_type, &value))
	{
		if (sscanf(value.addr, "%i", &number_of_times) != 1)
			number_of_times = 0;
	}
  else
    number_of_times=0;

  number_of_times++;
  timefriend = new struct TIMEINFO [number_of_times+1];

 if (XrmGetResource(resource_db,"bbtime.my.name","Bbtime.My.Name",
                    &value_type, &value)) {
    int len=strlen(value.addr);
    
    timefriend[0].name = new char [len +2 + time_len];
    memset(timefriend[0].name,0,len+2+time_len);
    strncpy(timefriend[0].name,value.addr,len);
  }
  else {
    int len=strlen("/me");
    timefriend[0].name = new char [len +2 + time_len];
    memset(timefriend[0].name,0,len+2+time_len);
    strncpy(timefriend[0].name,"/me",len);
  }
  timefriend[0].offset=0; 
  
  for (i=1;i<number_of_times;i++) {
    getTimeInfo(i);
  }
}
