// bbtime.cc for bbtime - an tool for display the time in X11.
//
//  Copyright (c) 1998-1999 by John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//

#include "bbtime.hh"
#include "version.h"

#include <time.h>
ToolWindow::ToolWindow(int argc,char **argv) :
  Basewindow(argc,argv)
{

  XrmInitialize();

 	resource = new Resource(this);
  date=False;
  selected_friend=0;
	MakeWindow(False);
  mbmenu->setHighlight(0);
  CheckTime(False);
  Redraw();
}

ToolWindow::~ToolWindow()
{
	XUnmapWindow(dpy,framewin);

	/* destroy pixmaps */
	if (pixmap.frame) image_control->removeImage(pixmap.frame);
	if (pixmap.label) image_control->removeImage(pixmap.label);

 	if (frameGC) XFreeGC(dpy,frameGC);
  if (menuGC) XFreeGC(dpy,menuGC);
	if (menuHiBGGC) XFreeGC(dpy,menuHiBGGC);
	if (menuHiGC) XFreeGC(dpy,menuHiGC);
	if (menuFrameGC)   XFreeGC(dpy,menuFrameGC);
	
  /* destroy windows */
	XDestroyWindow(dpy,labelwin);
	XDestroyWindow(dpy,framewin);
}

void ToolWindow::Reconfigure(void)
{
	/* destroy pixmaps */
	image_control->removeImage(pixmap.frame);
	image_control->removeImage(pixmap.label);
	mbmenu->ClearMenu();

	resource->Reload();

	MakeWindow(True);
  CheckTime(False);

 	XClearWindow(dpy, labelwin);
	XClearWindow(dpy, framewin);
	Redraw();
}

unsigned int ToolWindow::bevelWidth()
{
	return resource->frame.bevelWidth;
}
	
XFontStruct *ToolWindow::titleFont()
{
	return resource->menu.font;
}
	
XFontStruct *ToolWindow::menuFont()
{
	return resource->menu.font;
}

int ToolWindow::MenuJustification()
{
	return resource->menu.justify;
}	

void ToolWindow::saveMenuSearch(Window window,Basemenu *menu)
{
	menuwin=window;
}

void ToolWindow::removeMenuSearch(Window window)
{
	menuwin= (Window )NULL;
}

Resource *ToolWindow::getResource()
{
  return resource;
}

void ToolWindow::MakeWindow(bool reconfigure)
{
	XSetWindowAttributes attrib;
	XWMHints wmhints;

  unsigned long create_mask = CWBackPixmap|
															CWOverrideRedirect |CWCursor|CWEventMask; 

#ifdef    HAVE_STRFTIME
  time_t ttmp = time(NULL);
  tt = 0;
  
  if (ttmp != -1) {
    tt = localtime(&ttmp);
    if (tt) {
      char t[1024], *time_string = (char *) 0;
      int i, len = strftime(t, 1024,
                        resource->report.strftimeFormat,tt);
      
      time_string = new char[len + 1];

      for (i = 0; i < len; i++)
      	*(time_string + i) = '0';
      *(time_string + len) = '\0';
      
      label.width = XTextWidth(resource->label.font, time_string, len) +
    	(resource->frame.bevelWidth * 2);

      delete [] time_string;
    } else {
      fprintf(stderr,"can't get the time \n\n");
      exit(-1);
    }
  } else {
      fprintf(stderr,"can't get the time \n\n");
    exit(-1);
  }
#else // !HAVE_STRFTIME
  label.width = XTextWidth(resource->label.font, "00:00000",
			     strlen("00:00000")) + (resource->frame.bevelWidth * 2);
#endif // HAVE_STRFTIME
 
   if (withdrawn&&label.width<64)
     label.width=64-2*resource->frame.bevelWidth;
           
  if (withdrawn)
    frame.width=64;
  else
  	frame.width=label.width + 2*resource->frame.bevelWidth;
	frame.height=resource->label.font->ascent+
          resource->label.font->descent+4*resource->frame.bevelWidth;
	label.height=frame.height-2*resource->frame.bevelWidth;

	if (resource->position.mask & XNegative) 
	{
		resource->position.x = display_width + resource->position.x - frame.width;
	}
	if (resource->position.mask & YNegative)
	{
		resource->position.y = display_height + resource->position.y - frame.height;
	}

	if (!withdrawn)
	{
		frame.x=resource->position.x;
		frame.y=resource->position.y;
	}
  
	if (withdrawn)
	{
		attrib.override_redirect = False;
   	wmhints.initial_state = WithdrawnState;
  }
	else
	{
		attrib.override_redirect = True;
	  wmhints.initial_state = NormalState;
	}

	attrib.background_pixmap = ParentRelative;

  pixmap.frame = image_control->renderImage(frame.width, frame.height,
                                          &resource->frame.texture);
			  								
  pixmap.label =	image_control->renderImage(label.width, label.height,
							          							     &resource->label.texture);
											
	 attrib.cursor = cursor;
	 attrib.event_mask = ButtonPressMask | ButtonReleaseMask | ExposureMask |
									     FocusChangeMask | KeyPressMask | StructureNotifyMask;

	if (!reconfigure)
  {
    framewin = XCreateWindow(dpy, root, resource->position.x, 
                            resource->position.y, frame.width,
	  										 		frame.height, 0, depth, InputOutput,
												 		v, create_mask, &attrib);
   }
  else if (!withdrawn)
  {
    	XMoveResizeWindow(dpy, framewin, resource->position.x, 
                        resource->position.y, frame.width,
	  										frame.height);

  }
  else
  {
    	XResizeWindow(dpy,framewin,frame.width,frame.height);
  }

	wmhints.flags = IconWindowHint | StateHint;
	wmhints.icon_window = framewin;
	XSetWMHints(dpy, framewin, &wmhints);

  if (!shape)
	{
		XSetWindowBackgroundPixmap(dpy, framewin, pixmap.frame);
	}


  if (!reconfigure)
  {
	  labelwin = XCreateWindow(dpy, framewin, resource->frame.bevelWidth, 
		  											 resource->frame.bevelWidth, 
                             label.width, label.height, 0,
	  												 depth, InputOutput, v, create_mask, &attrib);
   }
  else
  {
   	XMoveResizeWindow(dpy, labelwin, resource->frame.bevelWidth, 
  									  resource->frame.bevelWidth,
                      label.width, label.height);

  }

  if (!resource->label.transparent)
  	XSetWindowBackgroundPixmap(dpy, labelwin, pixmap.label);

  if (!reconfigure)
  {
  	gcv.font = resource->label.font->fid;
	  gcv.foreground = resource->label.textColor.pixel;
  	frameGC = XCreateGC(dpy, framewin,GCFont|GCForeground, &gcv);

    gcv.font = resource->menu.font->fid;                    	
   	gcv.foreground = resource->menu.texture.color.pixel;
	  menuGC = XCreateGC(dpy, framewin,GCFont|GCForeground, &gcv);

	  gcv.foreground = resource->menu.highlightColor.pixel;
    gcv.arc_mode = ArcChord;
 		gcv.fill_style = FillSolid;
	 	menuHiBGGC = XCreateGC(dpy, framewin,GCForeground|
                           GCFillStyle|GCArcMode, &gcv);

	  gcv.foreground = resource->menu.hitextColor.pixel;	                    	
	  menuHiGC = XCreateGC(dpy, framewin,	GCFont|GCForeground, &gcv);

	  gcv.foreground = resource->menu.textColor.pixel;                    	
		menuFrameGC = XCreateGC(dpy, framewin,GCFont|GCForeground, &gcv);
                    	
   	mbmenu = new Timemenu(this);
		mbmenu->Update();
  }
  else
  {
  	gcv.font = resource->label.font->fid;
	  gcv.foreground = resource->label.textColor.pixel;
  	XChangeGC(dpy, frameGC,GCFont|GCForeground, &gcv);

		gcv.font = resource->menu.font->fid;                    	
   	gcv.foreground = resource->menu.texture.color.pixel;
	  XChangeGC(dpy, menuGC,GCFont|GCForeground, &gcv);
	
	  gcv.foreground = resource->menu.highlightColor.pixel;	
  	XChangeGC(dpy, menuHiBGGC,GCFont|GCForeground, &gcv);

	  gcv.foreground = resource->menu.hitextColor.pixel;	                    	
	  XChangeGC(dpy, menuHiGC, GCFont|GCForeground, &gcv);

	  gcv.foreground = resource->menu.textColor.pixel;                    	
		XChangeGC(dpy, menuFrameGC,GCFont|GCForeground, &gcv);
		
				
	  mbmenu->Reconfigure();

  }

  if (!reconfigure)
  {
	  XClearWindow(dpy, framewin);
    XMapWindow(dpy, framewin);  
    XMapSubwindows(dpy, framewin);
  }
	
}

void ToolWindow::CheckTime(bool redraw)
{
  time_t tmp;
  time_t ctmp;
  int i;
  
  if ((tmp = time(NULL)) != -1) {
    for (i=0;i<resource->number_of_times;i++)
			mbmenu->UpdateTime(tmp+resource->timefriend[i].offset,i);
    ctmp=tmp + resource->timefriend[selected_friend].offset;
    if (! (tt = localtime(&ctmp))) return;
    if (tt->tm_min != minute || tt->tm_hour != hour || redraw) {

      hour = tt->tm_hour;
      minute = tt->tm_min;
      XClearWindow(dpy, labelwin);
      Redraw();
    }
  }
}
  
void ToolWindow::Redraw()
{

#ifdef    HAVE_STRFTIME
  char t[1024];
  if (! strftime(t, 1024,resource->report.strftimeFormat, tt))
    return;
#else // !HAVE_STRFTIME
  char t[9];
  if (date) {
    // format the date... with special consideration for y2k ;)
    if (resource->report.euStyle)
      sprintf(t, "%02d.%02d.%02d", tt->tm_mday, tt->tm_mon + 1,
              (tt->tm_year >= 100) ? tt->tm_year - 100 : tt->tm_year);
    else
      sprintf(t, "%02d/%02d/%02d", tt->tm_mon + 1, tt->tm_mday,
              (tt->tm_year >= 100) ? tt->tm_year - 100 : tt->tm_year);
  } else {
    if (resource->report.clock24Hour)
    	sprintf(t, "  %02d:%02d ", hour, minute);
    else
     	sprintf(t, "%02d:%02d %cm",
   	((hour > 12) ? hour - 12 :
    ((hour == 0) ? 12 : hour)), minute,
   	((hour >= 12) ? 'p' : 'a'));
  }
#endif // HAVE_STRFTIME

  int len = strlen(t);
  unsigned int slen = XTextWidth(resource->label.font, t, len);
  XDrawString(dpy, labelwin, frameGC, (label.width - slen) / 2,
	           (label.height + resource->label.font->ascent -
            	resource->label.font->descent) / 2,t, len);

 	if (mbmenu->Visible() && mbmenu->WaitForUpdate())
		mbmenu->Update();

}


void ToolWindow::EventLoop(void) 
{
  int xfd = ConnectionNumber(dpy);
  time_t lastTime = time(NULL);
	bool shutdown=False;

  lastTime = ((lastTime / 60) * 60);

  while (! shutdown) 
	{
    if (XPending(dpy)) 
		{
      XEvent Event;
      XNextEvent(dpy, &Event);
			/* process events */
			switch (Event.type)
			{
				case Expose: 
				{
    				if (Event.xexpose.window==menuwin) 
	    			 	mbmenu->exposeEvent(&Event.xexpose);
            Redraw();
				}
				break;
				case ButtonPress:
				{
  				if (Event.xbutton.window == menuwin) {
				    mbmenu->buttonPressEvent(&Event.xbutton);
          }
          else 
					if (Event.xbutton.button == LEFT_BUTTON)
					{
            if (Event.xbutton.window == framewin) {
   						if (!(raised)) {
  							XRaiseWindow(dpy,framewin);
	  						raised=True;						
		  			  }
            }
            if (Event.xbutton.window == labelwin) {
              if (mbmenu->Visible())
                mbmenu->Hide();
              else {
                if (mbmenu->WaitForUpdate()) mbmenu->Update();
                mbmenu->Move(frame.x,frame.y,withdrawn);
                mbmenu->Show();
              }
            }
					}
					else if (Event.xbutton.button == MIDDLE_BUTTON)
					{
						if (raised) 
						{
							XLowerWindow(dpy,framewin);
							raised=False;
						}
					}
				}
				break;
      case ButtonRelease:
				{
					if (Event.xbutton.window == menuwin)
					{
					   mbmenu->buttonReleaseEvent(&Event.xbutton);
					}
        }
        break;
   		case MotionNotify:
			{
				if (Event.xmotion.window==menuwin)
				 	mbmenu->motionNotifyEvent(&Event.xmotion);
			}
      break;
   	 	case EnterNotify:
		 	{
		 		if (Event.xcrossing.window==menuwin)
		 		 	mbmenu->enterNotifyEvent(&Event.xcrossing);
		 	}
		 	break;
		 	case LeaveNotify:
		 	{
		 		if (Event.xcrossing.window==menuwin)
		 		 	mbmenu->leaveNotifyEvent(&Event.xcrossing);
		 	}
    	 	case ConfigureNotify:
	     	{
		 	   	if (Event.xconfigure.window==framewin && Event.xconfigure.send_event)
		 	 	  {
  					Reconfigure();
	  	 	 		int parent_x,parent_y;
		   	 		Window parent_root;
		 	   		unsigned int parent_width;
					  unsigned int parent_height;
				  	unsigned int parent_border_width;
  					unsigned int parent_depth;
	  	 	 		XGetGeometry(dpy,Event.xconfigure.above,&parent_root,
		 	 				            &parent_x,&parent_y,&parent_width,&parent_height,
		           	 				  &parent_border_width,&parent_depth);
    	 	 		frame.x=Event.xconfigure.x+parent_x;
		   	 		frame.y=Event.xconfigure.y+parent_y;
		 	   	}
  		 	}
			}
			
      if (time(NULL) - lastTime > 59) 
			{
				CheckTime(False);
				lastTime = time(NULL);
      }
    } 
		else
		{
      if (time(NULL) - lastTime < 60) 
			{
				fd_set rfds;
				FD_ZERO(&rfds);
				FD_SET(xfd, &rfds);
	
				struct timeval tv;
				tv.tv_sec = 60 - (time(NULL) - lastTime);
				tv.tv_usec = 0;
	
				select(xfd + 1, &rfds, 0, 0, &tv);
      } 
			else
			{
				CheckTime(False);
				lastTime = time(NULL);
      }
    }
  }
}

int main(int argc,char **argv)
{
	ToolWindow *AppWindow = new ToolWindow (argc,argv) ;

	AppWindow->EventLoop();
	delete AppWindow;
}
